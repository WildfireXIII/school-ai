// WorldRunner.h

#ifndef WORLD_RUNNER_H
#define WORLD_RUNNER_H

#include "Percept.h"
#include "WumpusWorld.h"
#include "Action.h"
#include "Agent.h"

#define MAX_MOVES_PER_GAME 1000
//#define MAX_MOVES_PER_GAME 50

class WorldRunner
{
public:
	static char* worldFile;
	static bool worldSet;
	
	// returns average score
	static int runWorld(Agent* agent, int trials, bool headless);
	static int runWorld(Agent* agent, int trials, bool headless, char* worldFilePath);
};

#endif // WORLD_RUNNER_H
