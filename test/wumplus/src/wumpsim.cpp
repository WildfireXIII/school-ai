// wumpsim.cpp
//
// Main Wumpus Simulator procedure.

#include <cmath>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <iostream>

#include "Percept.h"
#include "Action.h"
#include "WumpusWorld.h"
#include "wumpsim.h"

#include "Agent.h"
#include "RandomAgent.h"
#include "HumanAgent.h"
#include "LogicalAgent.h"

#include "WorldRunner.h"

using namespace std;

int main (int argc, char *argv[])
{
	int worldSize = 10;
	int numTrials = 1;
	int numTries = 1;
	unsigned seed;
	char* worldFile;
	bool seedSet = false;
	bool worldSet = false;

	string agentType = "default";
	string mode = "normal";

	// Process command-line options
	int i = 1;
	while (i < argc)
	{
		if (strcmp(argv[i], "-size") == 0)
		{
			i++;
			worldSize = atoi(argv[i]);
			if (worldSize < 2) { worldSize = 2; }
		} 
		else if (strcmp(argv[i], "-mode") == 0)
		{
			i++;
			mode = argv[i];
		}
		else if (strcmp(argv[i], "-trials") == 0)
		{
			i++;
			numTrials = atoi(argv[i]);
		} 
		else if (strcmp(argv[i], "-tries") == 0)
		{
			i++;
			numTries = atoi(argv[i]);
		} 
		else if (strcmp(argv[i], "-seed") == 0)
		{
			i++;
			seed = atoi(argv[i]);
			seedSet = true;
		} 
		else if (strcmp(argv[i], "-world") == 0)
		{
			i++;
			worldFile = argv[i];
			if(strcmp(worldFile, "default"))
			{
				worldSet = true;
			}
			
		} 
		else if (strcmp(argv[i], "-agent") == 0)
		{
			i++;
			agentType = argv[i];
		}
		else 
		{
			cout << "unknown option " << argv[i] << endl;
			exit(1);
		}
		i++;
	}

	// Set random number generator seed
	if (!seedSet) { seed = (unsigned) time (0); }
	srand (seed);

	vector<int> scores = vector<int>();

	// Print welcome
	cout << "Welcome to the Wumpus World Simulator v";
        cout << WUMPSIM_VERSION << ".  Happy hunting!" << endl << endl;
		
	// SIMPLE
	if (mode == "normal")
	{
		Agent* agent = NULL;
		int score;	
		// choose which agent
		if (agentType == "default") 
		{ 
			cout << "Creating a human-interface agent" << endl;
			agent = new HumanAgent(); 
		}
		else if (agentType == "random") 
		{ 
			//cout << "Creating a random-move agent" << endl;
			agent = new RandomAgent(); 
		}
		else if (agentType == "logical")
		{
			agent = new LogicalAgent();
		}
		if(worldSet) { score = WorldRunner::runWorld(agent, 1, false, worldFile); }
		else { score = WorldRunner::runWorld(agent, 1, false); }
		
		//int score = WorldRunner::runWorld(agent, 1000, true);
		cout << score << endl;
	}

	// METRICS
	else if (mode == "metrics")
	{
		for (int i = 0; i < 1000; i++)
		{
			Agent* agent = NULL;
				
			// choose which agent
			if (agentType == "default") { agent = new HumanAgent(); }
			else if (agentType == "random") { agent = new RandomAgent(); }
			else if (agentType == "logical") { agent = new LogicalAgent(); }

			int score = WorldRunner::runWorld(agent, 100, false);
			scores.push_back(score);
			cout << "." << flush;
			//cout << i << " " << score << endl;
		}
		cout << endl;

		// calculate standard deviation
		int total = 0;
		for (int i = 0; i < scores.size(); i++) { total += scores[i]; }
		//cout << "Total: " << total << endl;
		int mean = (int)((float) total / (float) scores.size());
		cout << "Average: " << mean << endl;
		vector<int> meansubtracted = vector<int>();
		for (int i = 0; i < scores.size(); i++) 
		{ 
			int temp = scores[i] - mean;
			temp *= temp;
			meansubtracted.push_back(temp);
		}
		total = 0;
		for (int i = 0; i < meansubtracted.size(); i++) { total += meansubtracted[i]; }
		mean = sqrt(total / meansubtracted.size());
		cout << "Standard dev: " << mean << endl;
	}	

	cout << "done" << endl;
	return 0;
}
