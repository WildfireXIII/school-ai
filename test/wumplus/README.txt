/-------------------------------------/
-------------GROUP MEMBERS------------
/-------------------------------------/
Nathan Martindale
Jacob Smith


/-------------------------------------/
-----------ORIGINAL CODEBASE----------
/-------------------------------------/

original wumpus code obtained from 

http://www.eecs.wsu.edu/~holder/courses/AI/wumpus/

/-------------------------------------/
-----------INPUT FILE LAYOUT----------
/-------------------------------------/

size N				EX.		size 5
wumpus x y					wumpus 5 5
gold x y					gold 4 5
supmuw x y					supmuw 3 5
pit x y						pit 3 5
wall x y					pit 2 5
							pit 1 5
							wall 1 2
							wall 2 2
							wall 3 2
							wall 4 2
							wall 5 4
							wall 4 4
							wall 3 4
							wall 2 4
							wall 1 4

/-------------------------------------/
--------------HOW TO RUN--------------
/-------------------------------------/
**input world files need to be in "wumplus-world" folder (project root directory)**

-if building and running from scratch in shell-
make run AGENT=logical FILE=filename.txt

-from executable we provided in submission-
bin/wumpsim -agent logical -world filename.txt

bitbucket link (if you want to directly clone)
https://bitbucket.org/WildfireXIII/wumplus-world.git

/-------------------------------------/
--------------ASSUMPTIONS-------------
/-------------------------------------/

Supmuw does not give food when in pit
Do not get gold points until you climb out with gold
Supmuw is still dangerous even if wumpus dies (because he is mad that he can smell him)
Cannot kill supmuw
Supmuw only gives food once
Wumpus can be in pit
Gold can be in pit

/-------------------------------------/
---------------STRATEGY---------------
/-------------------------------------/

Our agent is very cautious and will leave if there is not a guaranteed path to move forward.
If our agent has exhuasted all other options, he will shoot the wumpus.
Our agent loves supmuws and will try to get to them if he can.
Our agent refuses to use the A * algorithm, very very stubborn and unwilling in terms of that.
