// WorldState.h

#ifndef WORLDSTATE_H
#define WORLDSTATE_H

#include <vector>
#include "Location.h"
#include "Orientation.h"

class WorldState
{
public:
	int worldSize;
	Location agentLocation;
	Orientation agentOrientation;
	Location wumpusLocation;
	Location goldLocation;
	Location supmuwLocation;
	vector<Location> pitLocations;
	vector<Location> wallLocations;
	bool agentAlive;
	bool agentHasArrow;
	bool agentHasGold;
	bool agentInCave;
	bool wumpusAlive;
	bool agentHasFood;
};

#endif // WORLDSTATE_H
