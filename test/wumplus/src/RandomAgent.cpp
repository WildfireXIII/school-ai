// RandomAgent.cpp

#include "RandomAgent.h"
#include <iostream>
#include <cstdlib>

using namespace std;

RandomAgent::RandomAgent() {}
RandomAgent::~RandomAgent() {}

void RandomAgent::Initialize() {}

Action RandomAgent::Process(Percept& percept)
{
	Action action;

	int randChoice = rand() % 6 + 1;

	if (randChoice == 1) { action = GOFORWARD; }
	else if (randChoice == 2) { action = TURNLEFT; }
	else if (randChoice == 3) { action = TURNRIGHT; }
	else if (randChoice == 4) { action = GRAB; }
	else if (randChoice == 5) { action = SHOOT; }
	else if (randChoice == 6) { action = CLIMB; }
	else { cout << "ERROR" << endl; }

	return action;
}

void RandomAgent::GameOver(int score) {}

Agent* RandomAgent::clone() { return new RandomAgent(*this); }
