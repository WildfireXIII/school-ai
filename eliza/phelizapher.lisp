;;==========================================================================
;;
;; STARTER FILE FOR CSC 4240/5240 PROGRAM #1: Eliza
;;==========================================================================

;;----------------------------------------------------------------------------
;; eliza: top-level function which, when given a sentence (no
;; punctuation, please!), comes back with a response like you would.

( defun eliza ( sentence )
  ( respond ( change-pros sentence ) database ) )

;;----------------------------------------------------------------------------
;; change-pros: changes the pronouns of the sentence so that Eliza can
;; come back with the appropriately switched first and second person
;; references.

( defun change-pros ( sentence )
  ( cond 
    ( ( null sentence ) nil )
    ( ( equal ( car sentence ) 'you )
      ( cons 'I ( change-pros ( cdr sentence ) ) ) )
    ( ( equal ( car sentence ) 'your )
      ( cons 'my ( change-pros ( cdr sentence ) ) ) )
    ( ( equal ( car sentence ) 'yours )
      ( cons 'mine ( change-pros ( cdr sentence ) ) ) )
    ( ( equal ( car sentence ) 'I )
      ( cons 'you ( change-pros ( cdr sentence ) ) ) )
    ( ( equal ( car sentence ) 'me )
      ( cons 'you ( change-pros ( cdr sentence ) ) ) )
    ( ( equal ( car sentence ) 'my )
      ( cons 'your ( change-pros ( cdr sentence ) ) ) )
    ( ( equal ( car sentence ) 'mine )
      ( cons 'yours ( change-pros ( cdr sentence ) ) ) )

    ;; CHANGE THIS: add more cases here of pronouns or other words
    ;; that should flip in order for this to work well

    ( t ( cons ( car sentence ) ( change-pros ( cdr sentence ) ) ) ) ) )

;;----------------------------------------------------------------------------
;; respond: given a sentence, looks through the database in search of
;; a matching pattern and the response; given the database response,
;; uses 'instantiate' to fill in the blanks, and returns the completed
;; response

( defun respond ( sentence db )
  ( cond
    ;; end of DB, return nil - should never really happen
    ( ( null db ) nil )

    ;; if the result of matching the sentence against the current
    ;; pattern is a success, produce this response
	
    ;( ( success ( setq result ( match sentence ( first ( car db ) ) ) ) )
      ;( instantiate result ( second ( car db ) ) ) )
	  
	;; NOTE - I changed this so that it would randomly select a response from a
	;; list of possible responses in the database associated with that pattern
    ( ( success ( setq result ( match sentence ( first ( car db ) ) ) ) )
      ( instantiate result (nth (random (length (second (car db))))  (second (car db)))))

    ;; otherwise, keep looking through the DB
    ( t ( respond sentence ( cdr db ) ) ) ) )

;;----------------------------------------------------------------------------
;; match: if there is not a match between this pattern and this data,
;; returns 'fail;' otherwise, returns the sentence in partitioned
;; format

( defun match ( data pattern )
  ( cond
    ;; end of both data and pattern; a match
    ( ( and ( null data ) ( null pattern ) ) nil )

    ;; end of pattern, but not end of data; no match
    ( ( null pattern ) fail )

    ;; end of data, but not end of pattern; if the pattern starts with
    ;; a variable, eat it and try and match the rest of the pattern to
    ;; the null sentence (will only work if all variables); otherwise,
    ;; fail
    ( ( null data ) 
      ( cond
	( ( variablep ( car pattern ) )
	  ( if ( success ( setq result ( match data ( cdr pattern ) ) ) )
	      result
	    fail ) )
	( t fail ) ) )


    ;; first item of data and pattern are identical; if the rest of it
    ;; matched, return the first item cons'ed with the rest of the
    ;; partitioned sentence; otherwise, fail
    ( ( equal ( car data ) ( car pattern ) )
      ( if ( success ( setq result ( match ( cdr data ) ( cdr pattern ) ) ) )
	  ( cons ( list ( car data ) ) result )
	fail ) )

    ;; first item of pattern is a variable; if the rest of the data
    ;; (minus the first word, matched to the variable) is a match with
    ;; all of the pattern, return the appropriate stuff; if all of the
    ;; data (variable eats nothing) matches the rest of the pattern,
    ;; return appropriate stuff; else, fail.
    ( ( variablep ( car pattern ) ) 
      ( cond
	;; variable eats nothing;  () is put in partitioned sentence
	( ( success ( setq result ( match data ( cdr pattern ) ) ) )
	  ( cons () result ) )
	;; variable eats one word; word is cons'ed into the first
	;; element of the partitioned sentence, assuming that the step
	;; before an actual match word would be a ()
	( ( success ( setq result ( match ( cdr data ) pattern ) ) )
	  ( cons ( cons ( car data ) ( car result ) ) ( cdr result ) ) )
	;; otherwise, fail
	( t fail ) ) )

    ( t fail ) ) )

;;----------------------------------------------------------------------------
;; instantiate: takes a partitioned sentence and the response it has
;; been matched to and generates the appropriated completed response

( defun instantiate ( partitioned response )
  ( cond
    ( ( null response ) nil )
    ;; numbers indicate what part of the partitioned sentence to
    ;; insert into the response
    ( ( numberp ( car response ) )
      ( setq index ( - ( car response ) 1 ) )
      ( append ( nth index partitioned )
	     ( instantiate partitioned ( cdr response ) ) ) )
    ( t ( cons ( car response )
	     ( instantiate partitioned ( cdr response ) ) ) ) ) )

;;---------------------------------------------------------------------------
;;
;;  			     helping functions
;;
;;---------------------------------------------------------------------------

( setq fail '-1 )

( defun success ( result )
  ( not ( equal result fail ) ) )

( defun variablep ( word )
  ( equal word '0 ) )


;;---------------------------------------------------------------------------
;;
;;  			         database
;;
;;---------------------------------------------------------------------------

;; CHANGE THIS: add more to this database so that the interaction is
;; more interesting and communicative and so that Eliza sounds like you 
;; would sound in the same conversation!
;;---------------------------------------------------------------------------


;; NOTE - Since I wanted patterns to have mulitple potential responses to
;; draw from, I made each concept its own miniature database that can then be 
;; referenced from more than one pattern inside the main database

;; ETHICS SYSTEMS
(setq db-goodbad
	  `(
		(Good and bad are somewhat relative no?)
		(Good and bad are subjective - depending on your ethics system - which do you follow?)
	  ))

(setq db-ethics
	  `(
		(Determing right and wrong is a hard problem. What ethics model do you subscribe to?)
		(A question of ethics. How do you feel about deontological ethics like kantianism?)
		(I guess this really all depends on whether you like to view things through kantianism utilitarianism or relativism)
		))

(setq db-utilitarianism
	  `(
		(Utilitarianism! Now that is an ethics system I can get behind! Happiness! Happiness for everyone!)
		(There we go - utilitarianism - certainly an interesting system to discuss - what do you feel is the greatest downside to implementing policies attempting to maximize happiness?)
		))

(setq db-kantianism
	  `(
		(Mr. Emanual Kant - an interesting guy to be sure. I have always found his lack of conflict resolution to be a little off-putting - what do you think?)
		(Ah yes - kantianism - deontological ethics systems have never quite been my thing - I can respect the golden rule and all that but I do appreciate a better a system for handling conflicts between priorities)
		))

(setq db-relativism
	  `(
		(Relativism eh? Certainly valid aspects of it - it is interesting to consider if there are really any moral aspects that are truly universal. What do you think?)
		))

;; BELIEF SYSTEMS
(setq db-materialism
	  `(
		(Yes - materialism - the belief that reality consists solely of physical matter. What do you think?)
		(Interesting - what do you think about materialism?)
		))

(setq db-theism
	  `(
		(Ahh theism - what would you say are the most important aspects of theism?)
		(Philosophers frequently argue about the impacts of theism on society - what do you think?)
		))

(setq db-empiricism
	  `(
		(An evidence-based approach is certainly a commendable one. Even if all we have is our own subjective reality in which to live - at least empiricism is a logically sound and systematic way of exploring that reality.)
		(Yep yep - empiricism - a good philosophy to live by for certain. Do you believe there are things we can know without taking an empircal approach towards it?)
		))

;; BEGINNING THINGS
(setq db-greetings
	   `(
		 (Hi! How is it going?)
		 (Hello there!)
		 (Hi there!)
		 (Hello!)))

(setq db-selfdescriptors
	  `(
		(My name is phelizapher - I like to discuss philosophy!)
		(I am phelizapher - a deep and profound thinker)))

(setq db-howami
	`(
	  (It is a great day for philosophiznig - so I am great!)
	  (Every day in which I can think about things is a good day - I am doing well!)
	  ))

(setq db-conversation-starters-1
	  `(
		(Hmmm - what are the first words that come to your head when you hear philosophy?)
		(let us discuss ethics - do you think consequences of actions or intentions of actions are more important?)
		(Ethics might be fun to talk about - what is your favorite ethics system?)
		(Materialism versus theism is always an interesting discussion - what do you think about those two separate doctrines?)
		))

;; GENERIC TURN-AROUNDS
(setq db-dig-deeper-1
	  `(
		(Could you tell me more about why you think 4 ?)
		(Why do you say 4 ?)
		(Why do you think 4 ?)
		(Why do you believe 4 ?)
		(So - 4 - could you expand on that?)
		(Let us assume 4 - what follows from this being the case?)
		(4 - interesting thought - what do you think this means?)
		))

(setq db-dig-deeper-2
	  `(
		(An interesting thought - why do you say that?)
		(You say 1 ? What is your reasoning behind that?)
		(An intriguing opinion - what leads you to believe this?)
		))

;; MY OPINION
(setq db-mythoughts
	  `(
		(6 ? Well first tell me what you think)
		(6 ? An interesting topic - before I bias you - what do you think?)
		(6 ? There are certainly points to both sides...what would you say is the actual core of the matter?)
		(6 ? I find there are multiple valid perspectives - is there a particular side you would like to debate?)
		))

(setq db-twosides
	  `(
		(4 or 6 ? For something like this I think it is best to look at the pros and cons of both. What would you say is 8 about 4 ?)
		(4 or 6 ? For something like this I think it is best to look at the pros and cons of both. What would you say is 8 about 6 ?)
		(8 is a bit of a subjective concept - 4 and 6 both have aspects of that - perhaps clarify some?)
		(I would argue that both 4 and 6 have some properties of being 8 to some extent. What do you think?)
		))

(setq db-ami
	`(
	  (4 ? Me ? Good heavens - why do you want to know?)
	  (Hmm - am I 4 ? That is an excellent question. I will devote more thinking time to answering this.)
	  (Am I 4 ? Oh my. Perhaps ? What does it really mean to be 4 ?)
	  (Huh. 4 ? I never actually considered that before. Maybe I am!)
	  (Well - what would you say is the fundamental property that makes something 4 ?)
	  ))

(setq db-myphilosophy
	  `(
		(My philosophy is a combination of utilitarianism - materialism/empiricism - and rationality - what is yours?)
		(I aspire to rationality and empiricism - what do you think about empiricism?)
		))

;; RANDOM KEYWORD MENTIONS
(setq db-notsure
	  `(
		(Very socratic of you - he profoundly stated that the only thing he knew was that he did not know anything)
		(Socrates would be proud - he was all about how the only thing he knew was that he did not know anything)
	  ))

(setq db-curious
	  `(
		(Yes - curiousity - an excellent virtue to have!)
		(If only more people were as curious as you)
		))

(setq db-why
	  `(
		(Why - that is the fundamental question. Why indeed?)
		(Why? Here we go - we are getting to the real root of the discussion here - why do you think?)
		))

;; END
(setq db-goodbye
	`(
	  (Cya! Remember - never be afraid to question things and actually think about them!)
	  (Have a nice day - keep questioning reality!)
	  ))
	  
( setf database
       `(

	; greetings
	 ( (Hello 0)
	  ,db-greetings)
	 ( (Hi 0)
	  ,db-greetings)

	; self descriptors
	( (What are I 0)
	 ,db-selfdescriptors)
	( (Who are I 0)
	 ,db-selfdescriptors)
	( (What is my name 0)
	 ,db-selfdescriptors)

	( (0 how are I 0)
	 ,db-howami)

	( (0 are I 0)
	 ,db-ami)

	( (what is my philosophy)
	 ,db-myphilosophy)

	; conversation starters
	( (Your not sure what to say)
	 ,db-conversation-starters-1)
	( (What do I want to talk about)
	 ,db-conversation-starters-1)
	( (What would I like to discuss)
	 ,db-conversation-starters-1)
	( (What do I want to discuss)
	 ,db-conversation-starters-1)
	( (What should we talk about)
	 ,db-conversation-starters-1)
	( (What should we discuss)
	 ,db-conversation-starters-1)

	; comparing two things
	( (do I think 0 or 0 is 0)
	 ,db-twosides)
	( (do I believe 0 or 0 is 0)
	 ,db-twosides)
	( (would I say 0 or 0 is 0)
	 ,db-twosides)

	; find out more
	( (0 you think 0)
	,db-dig-deeper-1)
	( (0 you believe 0)
	 ,db-dig-deeper-1)
	( (In your opinion 0)
	 ,db-dig-deeper-1)
	( (You would say 0)
	 ,db-dig-deeper-1)
	( (You find that 0)
	 ,db-dig-deeper-1)
	 
	( (0 in your opinion)
	 ,db-dig-deeper-2)

	; what does phelizapher think
	( (what are my beliefs on 0)
	 ,db-mythoughts)
	( (what are my thoughts on 0)
	 ,db-mythoughts)
	( (what do I think about 0)
	 ,db-mythoughts)

	( (You am not sure 0)
	 ,db-notsure)
	( (You do not know 0)
	 ,db-notsure)

	; systems of belief
	( (0 materialism 0)
	 ,db-materialism)
	( (0 theism 0)
	 ,db-theism)
	( (0 god 0)
	 ,db-theism)
	( (0 religion 0)
	 ,db-theism)

	; ethics things
	( (0 good 0)
	 ,db-goodbad)
	( (0 bad 0)
	 ,db-goodbad)

	( (0 morals 0)
	 ,db-ethics)
	( (0 morality 0)
	 ,db-ethics)
	( (0 right 0)
	 ,db-ethics)
	( (0 wrong 0)
	 ,db-ethics)

	( (0 curious 0)
	 ,db-curious)
	( (0 curiousity 0)
	 ,db-curious)

	; ethics systems
	( (0 kantianism 0)
	 ,db-kantianism)
	( (0 universal 0)
	 ,db-kantianism)
	( (0 utilitarianism 0)
	 ,db-utilitarianism)
	( (0 relativism 0)
	 ,db-relativism)

	; science/empircism keywords
	( (0 scientific 0)
	 ,db-empiricism)
	( (0 science 0)
	 ,db-empiricism)
	( (0 evidence 0)
	 ,db-empiricism)
	( (0 empirical 0)
	 ,db-empiricism)
	( (0 empiricism 0)
	 ,db-empiricism)

	( (why 0)
	 ,db-why)
		 
	; goodbyes
	 ( (0 goodbye 0)
	   ,db-goodbye )
	 ( (0 bye 0)
	   ,db-goodbye )

	 ;; the catch-alls
	 ( (0) 
	   (
		(Could you expand on that some?)
		(I am not sure I follow you - what do you mean?)
		(Huh? Oh sorry - I was busy phelizaphizing - say what now?)
		(We need to dig deeper into this discussion - what is really at the core of what you are saying?)
		(What would be another way of saying that?)
		(I am not sure if I am understanding you correctly - could you rephrase?)
		) ) ) )
