(defun tower-of-hanoi (n) (transfer 'a 'b 'c n))

(defun move-disk (from to n)
  (list (list 'move 'disk n 'from from 'to to)))

(defun transfer (from to spare n)
  (cond ((= n 1) (move-disk from to n))
		(t (append (transfer from
							 spare
							 to
							 (- n 1))
				   (move-disk from to n)
				   (transfer spare
							 to
							 from
							 (- n 1))))))

;(defun start-pole (n name)
;  (cons 'a (list (start-pole-recurse n ()))))

;(defun start-pole (n name)
  ;(list 'a (start-pole-recurse n ())))
	;
;(defun start-pole-recurse (n building-list)
  ;(if (> n 0)
	;(start-pole-recurse (- n 1) (append building-list (list (+ (length building-list) 1))))
	;building-list))
;
;(defun tower-of-hanoi (n) (transfer (start-pole n 'a) '(b ()) '(c ()) n))
;
;(defun list-state (a b c)
  ;(list (list a b c)))
;
;(defun change-state (a b c from to)
  ;
;
;(defun transfer (from to spare n)
  ;(cond ((= n 0) (list 'complete?))
		;(t (append (
