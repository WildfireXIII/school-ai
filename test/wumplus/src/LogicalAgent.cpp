// LogicalAgent.cpp
 
#include "LogicalAgent.h"
#include <iostream>

using namespace std;

LogicalAgent::LogicalAgent() { }
LogicalAgent::~LogicalAgent() { }

void LogicalAgent::Initialize() 
{
	this->turn = 0;
	this->x = 1;
	this->y = 1;
	this->heading = 1;

	this->knownPitLocations = vector<Location>();
	this->potentialPitLocations = vector<Location>();
	this->breezeLocations = vector<Location>();

	this->potentialWumpusLocations = vector<Location>();
	this->stenchLocations = vector<Location>();

	this->potentialSupmuwLocations = vector<Location>();
	
	this->knownWalls = vector<Location>();
	this->safeFrontier = vector<Location>();
	this->safeVisited = vector<Location>();

	this->safeFrontier.push_back(Location(1, 1));
}

// check if the list has the location in it
bool LogicalAgent::doListHazLoc(vector<Location> list, Location loc)
{
	for (int i = 0; i < list.size(); i++) { if (list[i] == loc) { return true; } }
	return false;
}

// remove the specified location from the passed list
void LogicalAgent::removeFromList(vector<Location>* list, Location loc)
{
	int index = -1;
	for (int i = 0; i < list->size(); i++) { if ((*list)[i] == loc) { index = i; break; } }

	if (index != -1) { list->erase(list->begin() + index); }
}

// manhattan distance
int LogicalAgent::Ldistance(int x_loc, int y_loc, int x_goal, int y_goal)
{

	int x_count = 0;
	int y_count = 0;

	while(x_loc != x_goal)
	{
		if(x_loc < x_goal)
		{
			x_loc++;
			x_count++;
		}
		else if(x_loc > x_goal)
		{
			x_loc--;
			x_count++;
		}
		else { break; }	
	}
	while(y_loc != y_goal)
	{
		if(y_loc < y_goal)
		{
			y_loc++;
			y_count++;
		}
		else if(y_loc > y_goal)
		{
			y_loc--;
			y_count++;
		}
		else { break; }	
	}

	return (x_count + y_count);
}


// -------------------------------------------------------
//		THE A* GRAVEYARD
//
//		We tried many, many different times to implement
//		a*, and despite several close results, kept
//		getting random memory issues and seg faults we
//		couldn't figure out.... the last method that we
//		actually use isn't exactly a*, but it at least
//		mostly works.
//
//		We leave the dead a* methods as a monument to
//		our many hours of wasted time.
// -------------------------------------------------------



/*vector<Location> LogicalAgent::greedyASTARthing(Location start, Location end, bool guarenteed)
{
	//cout << "goal " << end.X << "," << end.Y << endl;
	int dist_traveled = 0;

	vector<Location> currentOptions = safeAdjacents(start, guarenteed);
	vector<Location> wayOut = vector<Location>();
	Location tempLoc= Location();
	int min = 869483;
	int currDist = 0;

	int printedTimes = 0;

	//printList(currentOptions, "currentOptions");
	//if (currentOptions.size() == 0) { cout << "\t\tERROR, empty options" << endl; }
	while(!doListHazLoc(currentOptions, end) && currentOptions.size() > 0) //if we can't see the end location
	{
		min = 869483;
		for (int i = 0; i < currentOptions.size(); i++)//look at all the the current options
		{
			if(doListHazLoc(wayOut, currentOptions[i])) { currentOptions.erase(currentOptions.begin() + i);}
			currDist = wayOut.size() + Ldistance(currentOptions[i].X, currentOptions[i].Y, end.X, end.Y);
			if(currDist < min) //find the best looking option
			{
				min = currDist;
				tempLoc = currentOptions[i];
			}
		}
		
		wayOut.push_back(tempLoc); //and add that to our path
	}
	//at this point it wil have broken out of the while loop meaning the end location is an option
	wayOut.push_back(end);
	//cout << "\t\treturning a*" << endl; //at this point it wil have broken out of the while loop meaning the end location is an option
	//move to that spot
	return wayOut;
}*/

/*vector<Location> LogicalAgent::greedyASTARthing(Location start, Location end, bool guarenteed)
{
	cout << "goal " << end.X << "," << end.Y << endl;


	Location frontier[100];
	int frontierSize = 0;
	Location localCameFrom[100];
	int localCameFromSize = 0;
	int frontierCosts[100];
	int frontierCostsSize = 0;
	int gCosts[100];
	int gCostsSize = 0;

	Location allReached[100];
	int allReachedSize = 0;
	Location cameFrom[100];
	int cameFromSize = 0;	

	
	allReached[0] = Location(start);
	allReachedSize++;
	cameFrom[0] = start;
	cameFromSize++;

	bool endFound = false;


	if (start == end) 
	{ 
		vector<Location> wayOut = vector<Location>();
		return wayOut; 
	}
	
	
	// expand frontier (intially just any adjacent)
	vector<Location> _frontier = safeAdjacents(start, guarenteed);
	for (int i = 0; i < _frontier.size(); i++) 
	{ 
		frontier[frontierSize] = Location(_frontier[i]);
		frontierSize++;
		
		//frontierCosts.push_back(-1);
		frontierCosts[frontierCostsSize] = -1;
		frontierCostsSize++;
		
		//gCosts.push_back(0);
		gCosts[gCostsSize] = 0;
		gCostsSize++;
		
		//localCameFrom.push_back(start);
		localCameFrom[localCameFromSize] = Location(start);
		localCameFromSize++;
	}
	//printList(frontier, "frontier");

	while (!endFound)
	{
		int minIndex = -1;
		int minCost = 100;
		
		// calc cost of all frontier options
		for (int i = 0; i < frontierSize; i++)
		{
			if (frontierCosts[i] != -1) { continue; } // don't recalculate
			
			Location loc = frontier[i];
			cout << "\tLooking at " << loc.X << "," << loc.Y << endl;

			if (loc == end)
			{
				//allReached.push_back(Location(loc.X, loc.Y));
				//cameFrom.push_back(Location(localCameFrom[i].X, localCameFrom[i].Y));
				allReached[allReachedSize] = Location(loc);
				allReachedSize++;

				cameFrom[cameFromSize] = Location(localCameFrom[i]);
				cameFromSize++;
				
				cout << "Found the end!" << endl;
				endFound = true;
				break;
			}
			
			// calculate costs
			frontierCosts[i] = gCosts[i] + 1 + Ldistance(loc.X, loc.Y, end.X, end.Y);
			cout << "\tcost: " << frontierCosts[i] << endl;
			if (frontierCosts[i] < minCost) 
			{
				minCost = frontierCosts[i];
				minIndex = i;
			}
		}

		if (endFound) { break; }

		// expand best option
		Location locForExpansion = frontier[minIndex];
		cout << "Choosing to expand " << locForExpansion.X << "," << locForExpansion.Y << endl;
		vector<Location> expansions = safeAdjacents(locForExpansion, guarenteed);
		printList(expansions, "expansions");
		
		// add expansions to frontier
		cout << "expansions size: " << expansions.size() << endl;
		for (int i = 0; i < expansions.size(); i++)
		{
			cout << "\ton expansion " << i << endl;
			//Location loc = expansions[i];
			Location loc = Location(expansions[i]);
			cout << "\t" << loc.X << "," << loc.Y << endl;

			//bool inAllReached = doListHazLoc(allReached, loc);
			bool inAllReached = false;
			for (int j = 0; j < allReachedSize; j++) { if (allReached[j] == loc) { inAllReached = true; break; } }
			cout << "was in all reached: " << inAllReached << endl;
			
			//bool inFrontier = doListHazLoc(frontier, loc);
			bool inFrontier = false;
			for (int j = 0; j < frontierSize; j++) { if (frontier[j] == loc) { inFrontier = true; break; } }
			cout << "was in frontier: "<< inFrontier << endl;
			
			if (!inAllReached && !inFrontier)
			{
				//cout << "frontier size: " << frontier.size() << endl;
				//frontier.push_back(Location(loc.X, loc.Y));
				//cout << "pushed to frontier" << endl;
				//frontierCosts.push_back(-1);
				//cout << "pushed to frontier costs" << endl;
				//gCosts.push_back(gCosts[minIndex] + 1);
				//cout << "pushed to gcosts" << endl;
				//localCameFrom.push_back(locForExpansion);
				//localCameFrom.push_back(Location(locForExpansion.X, locForExpansion.Y));
				//cout << "pushed to local came from" << endl;

				frontier[frontierSize] = Location(loc);
				frontierSize++;

				frontierCosts[frontierCostsSize] = -1;
				frontierCostsSize++;

				gCosts[gCostsSize] = gCosts[minIndex] + 1;
				gCostsSize++;

				localCameFrom[localCameFromSize] = Location(locForExpansion);
				localCameFromSize++;
			}
			cout << "\tafter condition" << endl;
		}
		cout << "after for loop" << endl;
		//printList(frontier, "frontier");
		
		// add current expandedfrontier from frontier (add to allReached) and
		// cameFrom from localCameFrom
		cout << "\tpushing back" << endl;
		//allReached.push_back(locForExpansion);
		//cameFrom.push_back(localCameFrom[minIndex]);
		allReached[allReachedSize] = Location(locForExpansion);
		allReachedSize++;
		cameFrom[cameFromSize] = Location(localCameFrom[minIndex]);
		cameFromSize++;
		
		// remove current expanded frontier and local camefrom
		cout << "\terasing..." << endl;
		//frontier.erase(frontier.begin()+minIndex);
		//frontierCosts.erase(frontierCosts.begin()+minIndex);
		//gCosts.erase(gCosts.begin()+minIndex);
		//localCameFrom.erase(localCameFrom.begin()+minIndex);
		for (int i = minIndex; i < frontierSize; i++) { frontier[i] = Location(frontier[i+1]); }
		frontierSize--;
		for (int i = minIndex; i < frontierCostsSize; i++) { frontierCosts[i] = frontierCosts[i+1]; }
		frontierCostsSize--;
		for (int i = minIndex; i < gCostsSize; i++) { gCosts[i] = gCosts[i+1]; }
		gCostsSize--;
		for (int i = minIndex; i < localCameFromSize; i++) { localCameFrom[i] = Location(localCameFrom[i+1]); }
		localCameFromSize--;
		
		cout << "\tend of while" << endl;
	}

	// reconstruct path
	vector<Location> wayOut = vector<Location>();
	
	wayOut.push_back(Location(allReached[allReachedSize - 1]));
	Location from = Location(cameFrom[cameFromSize - 1]);

	cout << "All reached" << endl;
	for (int i = 0; i < allReachedSize; i++)
	{
		cout << allReached[i].X << "," << allReached[i].Y << endl;
	}
	cout << "came from " << endl;
	for (int i =0 ; i < cameFromSize; i++)
	{
		cout << cameFrom[i].X << "," << cameFrom[i].Y << endl;
	}

	while (!(from == start))
	{
		// find from in allReached
		//for (int i = 0; i < allReached.size(); i++)
		for (int i = 0; i < allReachedSize; i++)
		{
			cout << "Checking " << from.X << "," << from.Y << " with " << allReached[i].X << "," << allReached[i].Y << endl;
			if (from == Location(0,0))
			{
				cout << "halt" << endl;
				cin.ignore();
				cin.clear();
			}
			if (allReached[i] == from)
			{
				cout << "it equals!" << endl;
				wayOut.insert(wayOut.begin(), Location(from));
				from = Location(cameFrom[i]);
				break;
			}
			cout << "doesn't equal" << endl;
		}
		//cout << "ERROR" << endl;
		printList(wayOut, "wayOut");
	}

	printList(wayOut, "wayOut");
	
	//frontier.clear();
	//frontierCosts.clear();
	//allReached.clear();
	//cameFrom.clear();
	//localCameFrom.clear();
	//gCosts.clear();
	
	return wayOut;
}*/


/*vector<Location> LogicalAgent::greedyASTARthing(Location start, Location end, bool guarenteed)
{
	cout << "goal " << end.X << "," << end.Y << endl;
	vector<Location> frontier = vector<Location>();
	vector<Location> localCameFrom = vector<Location>();
	vector<int> frontierCosts = vector<int>();
	vector<int> gCosts = vector<int>();
		
	vector<Location> allReached = vector<Location>();
	vector<Location> cameFrom = vector<Location>();

	allReached.push_back(start);
	cameFrom.push_back(start);

	bool endFound = false;


	if (start == end) { return frontier; }
	
	
	// expand frontier (intially just any adjacent)
	frontier = safeAdjacents(start, guarenteed);
	for (int i = 0; i < frontier.size(); i++) 
	{ 
		frontierCosts.push_back(-1);
		gCosts.push_back(0);
		localCameFrom.push_back(start);
	}
	printList(frontier, "frontier");

	while (!endFound)
	{
		cout << "frontier size: " << frontier.size() << endl;
		cout << "localCameFrom size: " << localCameFrom.size() << endl;
		cout << "frontierCosts size: " << frontierCosts.size() << endl;
		cout << "gCosts size: " << gCosts.size() << endl;
		int minIndex = -1;
		int minCost = 100;
		
		// calc cost of all frontier options
		for (int i = 0; i < frontier.size(); i++)
		{
			if (frontierCosts[i] != -1) { continue; } // don't recalculate
			
			Location loc = frontier[i];
			cout << "\tLooking at " << loc.X << "," << loc.Y << endl;

			if (loc == end)
			{
				allReached.push_back(Location(loc.X, loc.Y));
				cameFrom.push_back(Location(localCameFrom[i].X, localCameFrom[i].Y));
				cout << "Found the end!" << endl;
				endFound = true;
				break;
			}
			
			// calculate costs
			frontierCosts[i] = gCosts[i] + 1 + Ldistance(loc.X, loc.Y, end.X, end.Y);
			cout << "\tcost: " << frontierCosts[i] << endl;
			if (frontierCosts[i] < minCost) 
			{
				minCost = frontierCosts[i];
				minIndex = i;
			}
		}

		if (endFound) { break; }

		// expand best option
		Location locForExpansion = frontier[minIndex];
		cout << "Choosing to expand " << locForExpansion.X << "," << locForExpansion.Y << endl;
		vector<Location> expansions = safeAdjacents(locForExpansion, guarenteed);
		printList(expansions, "expansions");
		
		// add expansions to frontier
		cout << "expansions size: " << expansions.size() << endl;
		for (int i = 0; i < expansions.size(); i++)
		{
			cout << "\ton expansion " << i << endl;
			//Location loc = expansions[i];
			Location loc = expansions[i];
			cout << "\t" << loc.X << "," << loc.Y << endl;

			bool inAllReached = doListHazLoc(allReached, loc);
			cout << "was in all reached: " << inAllReached << endl;
			bool inFrontier = doListHazLoc(frontier, loc);
			cout << "was in frontier: "<< inFrontier << endl;
			
			if (!doListHazLoc(allReached, loc) && !doListHazLoc(frontier, loc))
			{
				cout << "frontier size: " << frontier.size() << endl;
				frontier.push_back(Location(loc.X, loc.Y));
				cout << "pushed to frontier" << endl;
				frontierCosts.push_back(-1);
				cout << "pushed to frontier costs" << endl;
				gCosts.push_back(gCosts[minIndex] + 1);
				cout << "pushed to gcosts" << endl;
				//localCameFrom.push_back(locForExpansion);
				localCameFrom.push_back(Location(locForExpansion.X, locForExpansion.Y));
				cout << "pushed to local came from" << endl;
			}
			cout << "\tafter condition" << endl;
		}
		cout << "after for loop" << endl;
		printList(frontier, "frontier");
		
		// add current expandedfrontier from frontier (add to allReached) and
		// cameFrom from localCameFrom
		cout << "\tpushing back" << endl;
		allReached.push_back(locForExpansion);
		cameFrom.push_back(localCameFrom[minIndex]);
		
		// remove current expanded frontier and local camefrom
		cout << "\terasing..." << endl;
		frontier.erase(frontier.begin()+minIndex);
		frontierCosts.erase(frontierCosts.begin()+minIndex);
		gCosts.erase(gCosts.begin()+minIndex);
		localCameFrom.erase(localCameFrom.begin()+minIndex);
		
		cout << "\tend of while" << endl;
	}

	// reconstruct path
	vector<Location> wayOut = vector<Location>();
	
	wayOut.push_back(allReached[allReached.size() - 1]);
	Location from = cameFrom[cameFrom.size() - 1];

	while (!(from == start))
	{
		// find from in allReached
		for (int i = 0; i < allReached.size(); i++)
		{
			if (allReached[i] == from)
			{
				wayOut.insert(wayOut.begin(), from);
				from = cameFrom[i];
				break;
			}
		}
		cout << "ERROR" << endl;
	}

	printList(wayOut, "wayOut");
	
	frontier.clear();
	frontierCosts.clear();
	allReached.clear();
	cameFrom.clear();
	localCameFrom.clear();
	gCosts.clear();
	
	return wayOut;
}

*/


vector<Location> LogicalAgent::greedyASTARthing(Location start, Location end, bool guarenteed)
{
	vector<Location> currentOptions = safeAdjacents(start, guarenteed);
	vector<vector<Location> > paths = vector<vector<Location > >();
	vector<Location> wayOut = vector<Location>();

	if (start == end) { return wayOut; }

	bool endFound = false;
	for (int i = 0; i < currentOptions.size(); i++)
	{
		vector<Location> path = vector<Location>();
		path.push_back(currentOptions[i]);
		paths.push_back(path);
		
		if (end == currentOptions[i]) 
		{ 
			endFound = true; 
			wayOut.push_back(end);
			break;
		}
	}

	int iterations = -1;
	while (!endFound)
	{
		iterations++;
		int pathIndexToExpand = -1;
		int minCost = 100;
		
		// choose cheapest to expand
		for (int i = paths.size() - 1; i >= 0; i--)
		{
			Location last = paths[i][paths[i].size() - 1];
			int cost = paths[i].size() + Ldistance(last.X, last.Y, end.X, end.Y);

			if (cost < minCost)
			{
				minCost = cost;
				pathIndexToExpand = i;
			}
		}

		// didn't find anything, must already be on the square?
		if (pathIndexToExpand == -1) { return wayOut; }
		
		Location last = paths[pathIndexToExpand][paths[pathIndexToExpand].size() - 1];

		currentOptions = safeAdjacents(last, guarenteed);
		int opIn = -1;
		int opMinCost = 100;
		for (int i = 0; i < currentOptions.size(); i++)
		{
			int opCost = paths[pathIndexToExpand].size() + Ldistance(currentOptions[i].X, currentOptions[i].Y, end.X, end.Y);

			if (opCost < opMinCost) 
			{ 
				opIn = i; 
				opMinCost = opCost;
			}
		}

		vector<Location> newPath = vector<Location>();
		for (int i = 0; i < paths[pathIndexToExpand].size(); i++)
		{
			newPath.push_back(paths[pathIndexToExpand][i]);
		}
		newPath.push_back(currentOptions[opIn]);
		paths.push_back(newPath);

		for (int i = 0; i < currentOptions.size(); i++)
		{
			// replace current (don't leave an already expanded option in the list
			if (i == (currentOptions.size() - 1)) { paths[pathIndexToExpand].push_back(currentOptions[i]); }
			// otherwise copy into new path
			else
			{
				if (doListHazLoc(paths[pathIndexToExpand], currentOptions[i])) { continue; }
				
				vector<Location> newPath = vector<Location>();
				for (int j = 0; j < paths[pathIndexToExpand].size(); j++)
				{
					newPath.push_back(paths[pathIndexToExpand][j]);
				}
				newPath.push_back(currentOptions[i]);
				paths.push_back(newPath);
			}
		}

		for (int i = 0; i < paths.size(); i++)
		{
			last = paths[i][paths[i].size() - 1];
			if (last == end)
			{
				endFound = true;
				wayOut = paths[i];
			}
		}
		if (iterations > 2000) { return paths[paths.size() - 1]; }
	}

	return wayOut;
}

void LogicalAgent::grabTheMoneyAndRun()
{
	this->op = "grab";
	this->stage = "grab";
	this->opInProgress = true;
}

vector<Location> LogicalAgent::safeAdjacents(Location loc, bool guarenteed)
{
	//if adjacent location is in safe frontier or saf visited, add it to our safe list
	vector<Location> safies = vector<Location>();

	if(doListHazLoc(this->safeVisited, Location(loc.X - 1, loc.Y)) || (doListHazLoc(this->safeFrontier, Location(loc.X - 1, loc.Y)) && !guarenteed))
		safies.push_back(Location(loc.X - 1, loc.Y));

	if(doListHazLoc(this->safeVisited, Location(loc.X + 1, loc.Y)) || (doListHazLoc(this->safeFrontier, Location(loc.X + 1, loc.Y)) && !guarenteed))
		safies.push_back(Location(loc.X + 1, loc.Y));

	if(doListHazLoc(this->safeVisited, Location(loc.X, loc.Y + 1)) || (doListHazLoc(this->safeFrontier, Location(loc.X, loc.Y + 1)) && !guarenteed))
		safies.push_back(Location(loc.X, loc.Y + 1));

	if(doListHazLoc(this->safeVisited, Location(loc.X, loc.Y - 1)) || (doListHazLoc(this->safeFrontier, Location(loc.X, loc.Y - 1)) && !guarenteed))
		safies.push_back(Location(loc.X, loc.Y - 1));

	return safies;
}

// handle encountering a wall
void LogicalAgent::owThatsAWall()
{
	Location lookingAt = Location(this->x, this->y);

	// no longer part of frontier
	removeFromList(&(this->safeFrontier), lookingAt);
	
	// move back
	if (this->heading == 0) { this->y--; }
	else if (this->heading == 1) { this->x--; }
	else if (this->heading == 2) { this->y++; }
	else if (this->heading == 3) { this->x++; }

	// record that we found a wall
	if (!doListHazLoc(this->knownWalls, lookingAt)) { this->knownWalls.push_back(Location(lookingAt)); }
}

// no safe way to continue exploring available, get out of here!
void LogicalAgent::flipTableImOut()
{
	this->op = "run";
	this->stage = "movement";

	this->targetLoc = Location(1,1);
	vector<Location> movements = greedyASTARthing(Location(this->x, this->y), this->targetLoc, true);
	this->movementPath = movements;
	this->strategy.clear();
	
	this->opInProgress = true;
}

// the wumpus is in the way of the shiny. KILL.
void LogicalAgent::wumpusGoinDown()
{
	this->op = "KILL";
	this->opInProgress = true;

	if (this->wumpusLocated) { this->target = this->knownWumpusLocation; }
	else { this->target = this->potentialWumpusLocations[0]; }

	this->targetLoc = findClosestSafeToTarget();
	this->stage = "movement";
	
	vector<Location> movements = greedyASTARthing(Location(this->x, this->y), this->targetLoc, false);
	this->movementPath = movements;
	this->strategy.clear();
}

// see if we can figure out location of the supmuw
void LogicalAgent::triangulateMoos()
{
	for (int i = 0; i < this->mooLocations.size(); i++)
	{
		Location loc = this->mooLocations[i];
		 
		// check if diagonal with any existing possible locs (meaning can
		// "triangulate" position)
		for (int j = 0; j < this->mooLocations.size(); j++)
		{
			Location otherLoc = this->mooLocations[j];
			
			// check if both axes are off by one (diagonal)
			if (abs(otherLoc.X - loc.X) == 1 && abs(otherLoc.Y - loc.Y) == 1)
			{
				// get two other inverted diagonal positions 
				Location diag1;
				Location diag2;

				if ((otherLoc.Y - loc.Y) == 1) { diag1 = Location(loc.X, loc.Y + 1); }
				else { diag1 = Location(loc.X, loc.Y - 1); }
					
				if ((otherLoc.X - loc.X) == 1) { diag2 = Location(loc.X + 1, loc.Y); }
				else { diag2 = Location(loc.X - 1, loc.Y); }
				
				// check if either have been visited (if one has, we know where the wumpus is at the other)
				if (doListHazLoc(this->safeVisited, diag1)) 
				{ 
					this->knownSupmuwLocation = Location(diag2.X, diag2.Y); 
					this->supmuwLocated = true;
				}
				else if (doListHazLoc(this->safeVisited, diag2)) 
				{ 
					this->knownSupmuwLocation = Location(diag1.X, diag1.Y); 
					this->supmuwLocated = true;
				}
			}
		}
	}
}

// see if we can figure out location of the wumpus
void LogicalAgent::triangulateStenches()
{
	for (int i = 0; i < this->stenchLocations.size(); i++)
	{
		Location loc = this->stenchLocations[i];
		 
		// check if diagonal with any existing possible locs (meaning can
		// "triangulate" position)
		for (int j = 0; j < this->stenchLocations.size(); j++)
		{
			Location otherLoc = this->stenchLocations[j];
			
			// check if both axes are off by one (diagonal)
			if (abs(otherLoc.X - loc.X) == 1 && abs(otherLoc.Y - loc.Y) == 1)
			{
				// get two other inverted diagonal positions 
				Location diag1;
				Location diag2;

				if ((otherLoc.Y - loc.Y) == 1) { diag1 = Location(loc.X, loc.Y + 1); }
				else { diag1 = Location(loc.X, loc.Y - 1); }
					
				if ((otherLoc.X - loc.X) == 1) { diag2 = Location(loc.X + 1, loc.Y); }
				else { diag2 = Location(loc.X - 1, loc.Y); }
				
				// check if either have been visited (if one has, we know where the wumpus is at the other)
				if (doListHazLoc(this->safeVisited, diag1)) 
				{ 
					this->knownWumpusLocation = Location(diag2.X, diag2.Y); 
					this->wumpusLocated = true;
				}
				else if (doListHazLoc(this->safeVisited, diag2)) 
				{ 
					this->knownWumpusLocation = Location(diag1.X, diag1.Y); 
					this->wumpusLocated = true;
				}
			}
		}
	}
}

// we found a new moo
void LogicalAgent::handleNewMoo(Location loc)
{
	this->mooLocations.push_back(loc);
	
	// check four surrounding areas
	vector<Location> newPossibles = vector<Location>();
	newPossibles.push_back(Location(loc.X - 1, loc.Y));
	newPossibles.push_back(Location(loc.X + 1, loc.Y));
	newPossibles.push_back(Location(loc.X, loc.Y + 1));
	newPossibles.push_back(Location(loc.X, loc.Y - 1));
	
	for (int i = 3; i >= 0; i--)
	{
		Location pos = newPossibles[i];
		bool remove = false;
		
		// check outside of map
		if (pos.X > 10 || pos.X < 1 || pos.Y > 10 || pos.Y < 1) { remove = true; }

		// check in safe frontier or visited
		if (doListHazLoc(this->safeFrontier, pos)) { remove = true; }
		if (doListHazLoc(this->safeVisited, pos)) { remove = true; }

		// wall
		if (doListHazLoc(this->knownWalls, pos)) { remove = true; }
		if (doListHazLoc(this->potentialSupmuwLocations, pos)) { remove = true; }
		
		if (remove) { newPossibles.erase(newPossibles.begin() + i); }
	}

	for (int i = 0; i < newPossibles.size(); i++) { this->potentialSupmuwLocations.push_back(Location(newPossibles[i])); }
}

// found a new stench
void LogicalAgent::handleNewStench(Location loc)
{
	this->stenchLocations.push_back(loc);
	
	// check four surrounding areas
	vector<Location> newPossibles = vector<Location>();
	newPossibles.push_back(Location(loc.X - 1, loc.Y));
	newPossibles.push_back(Location(loc.X + 1, loc.Y));
	newPossibles.push_back(Location(loc.X, loc.Y + 1));
	newPossibles.push_back(Location(loc.X, loc.Y - 1));
	
	for (int i = 3; i >= 0; i--)
	{
		Location pos = newPossibles[i];
		bool remove = false;
		
		// check outside of map
		if (pos.X > 10 || pos.X < 1 || pos.Y > 10 || pos.Y < 1) { remove = true; }

		// check in safe frontier or visited
		if (doListHazLoc(this->safeFrontier, pos)) { remove = true; }
		if (doListHazLoc(this->safeVisited, pos)) { remove = true; }

		// wall
		if (doListHazLoc(this->knownWalls, pos)) { remove = true; }
		if (doListHazLoc(this->potentialWumpusLocations, pos)) { remove = true; }
		
		if (remove) { newPossibles.erase(newPossibles.begin() + i); }
	}

	for (int i = 0; i < newPossibles.size(); i++) { this->potentialWumpusLocations.push_back(Location(newPossibles[i])); }
}

// found a new breeze
void LogicalAgent::handleNewBreeze(Location loc)
{
	// check four surrounding areas
	vector<Location> newPossibles = vector<Location>();
	newPossibles.push_back(Location(loc.X - 1, loc.Y));
	newPossibles.push_back(Location(loc.X + 1, loc.Y));
	newPossibles.push_back(Location(loc.X, loc.Y + 1));
	newPossibles.push_back(Location(loc.X, loc.Y - 1));
	
	for (int i = 3; i >= 0; i--)
	{
		Location pos = newPossibles[i];
		bool remove = false;
		
		// check outside of map
		if (pos.X > 10 || pos.X < 1 || pos.Y > 10 || pos.Y < 1) { remove = true; }

		// check in safe frontier or visited
		if (doListHazLoc(this->safeFrontier, pos)) { remove = true; }
		if (doListHazLoc(this->safeVisited, pos)) { remove = true; }
		
		// check wall
		if (doListHazLoc(this->knownWalls, pos)) { remove = true; }

		// check pre-existence
		if (doListHazLoc(this->potentialPitLocations, pos)) { remove = true; }

		if (remove) { newPossibles.erase(newPossibles.begin() + i); }
	}

	for (int i = 0; i < newPossibles.size(); i++) { this->potentialPitLocations.push_back(Location(newPossibles[i])); }
}

void LogicalAgent::removePotentialPits(Location loc)
{
	Location left = Location(loc.X - 1, loc.Y);
	Location right = Location(loc.X + 1, loc.Y);
	Location up = Location(loc.X, loc.Y + 1);
	Location down = Location(loc.X, loc.Y - 1);
	
	if (doListHazLoc(this->potentialPitLocations, left)) { removeFromList(&(this->potentialPitLocations), left); }
	if (doListHazLoc(this->potentialPitLocations, right)) { removeFromList(&(this->potentialPitLocations), right); }
	if (doListHazLoc(this->potentialPitLocations, up)) { removeFromList(&(this->potentialPitLocations), up); }
	if (doListHazLoc(this->potentialPitLocations, down)) { removeFromList(&(this->potentialPitLocations), down); }
}

void LogicalAgent::removePotentialWumpuses(Location loc)
{
	Location left = Location(loc.X - 1, loc.Y);
	Location right = Location(loc.X + 1, loc.Y);
	Location up = Location(loc.X, loc.Y + 1);
	Location down = Location(loc.X, loc.Y - 1);
	
	if (doListHazLoc(this->potentialWumpusLocations, left)) { removeFromList(&(this->potentialWumpusLocations), left); }
	if (doListHazLoc(this->potentialWumpusLocations, right)) { removeFromList(&(this->potentialWumpusLocations), right); }
	if (doListHazLoc(this->potentialWumpusLocations, up)) { removeFromList(&(this->potentialWumpusLocations), up); }
	if (doListHazLoc(this->potentialWumpusLocations, down)) { removeFromList(&(this->potentialWumpusLocations), down); }
}

void LogicalAgent::removePotentialSupmuws(Location loc)
{
	Location left = Location(loc.X - 1, loc.Y);
	Location right = Location(loc.X + 1, loc.Y);
	Location up = Location(loc.X, loc.Y + 1);
	Location down = Location(loc.X, loc.Y - 1);
	
	if (doListHazLoc(this->potentialSupmuwLocations, left)) { removeFromList(&(this->potentialSupmuwLocations), left); }
	if (doListHazLoc(this->potentialSupmuwLocations, right)) { removeFromList(&(this->potentialSupmuwLocations), right); }
	if (doListHazLoc(this->potentialSupmuwLocations, up)) { removeFromList(&(this->potentialSupmuwLocations), up); }
	if (doListHazLoc(this->potentialSupmuwLocations, down)) { removeFromList(&(this->potentialSupmuwLocations), down); }
}

void LogicalAgent::expandSafeFrontier(Location loc)
{
	vector<Location> newPossibles = vector<Location>();
	newPossibles.push_back(Location(loc.X - 1, loc.Y));
	newPossibles.push_back(Location(loc.X + 1, loc.Y));
	newPossibles.push_back(Location(loc.X, loc.Y + 1));
	newPossibles.push_back(Location(loc.X, loc.Y - 1));
	
	for (int i = 3; i >= 0; i--)
	{
		Location pos = newPossibles[i];
		bool remove = false;
		
		// check outside of map
		if (pos.X > 10 || pos.X < 1 || pos.Y > 10 || pos.Y < 1) { remove = true; }

		// check already in safe frontier or visited
		if (doListHazLoc(this->safeFrontier, pos)) { remove = true; }
		if (doListHazLoc(this->safeVisited, pos)) { remove = true; }

		// check if wall
		if (doListHazLoc(this->knownWalls, pos)) { remove = true; }

		if (pos == this->knownWumpusLocation) { remove = true; }
		
		if (remove) { newPossibles.erase(newPossibles.begin() + i); }
	}

	for (int i = 0; i < newPossibles.size(); i++) { this->safeFrontier.push_back(Location(newPossibles[i])); }
}

// NOTE: assumes that location is actually adjacent
// (determines all necessary rotations)
vector<Action>* LogicalAgent::calcMoveToAdjacent(Location loc)
{
	vector<Action>* actions = new vector<Action>();

	int goalHeading = 0;
	if (loc.X > this->x) { goalHeading = 1; }
	else if (loc.X < this->x) { goalHeading = 3; }
	else if (loc.Y > this->y) { goalHeading = 0; }
	else { goalHeading = 2; }

	// add appropriate turn directions
	if (this->heading != goalHeading)
	{
		int clockwiseDiff = 0; // number of right turns needed
		int counterDiff = 0; // number of left turns needed

		if (goalHeading < this->heading)
		{
			clockwiseDiff = (goalHeading + 4) - this->heading;
			counterDiff = this->heading - goalHeading;
		}
		else
		{
			clockwiseDiff = goalHeading - this->heading;
			counterDiff = (this->heading + 4) - goalHeading;
		}
		
		if (clockwiseDiff < counterDiff) { for (int i = 0; i < clockwiseDiff; i++) { actions->push_back(TURNRIGHT); } }
		else { for (int i = 0; i < counterDiff; i++) { actions->push_back(TURNLEFT); } }
	}
	
	actions->push_back(GOFORWARD);
	return actions;
}

Location LogicalAgent::findNearestSafeFrontier()
{
	int nearestDistance = 100;
	Location nearestLoc = Location(0,0);
	
	for (int i = 0; i < this->safeFrontier.size(); i++)
	{
		Location loc = this->safeFrontier[i];

		int distX = abs(loc.X - this->x);
		int distY = abs(loc.Y - this->y);

		int distance = distX + distY;

		if (distance < nearestDistance) 
		{ 
			nearestDistance = distance; 
			nearestLoc = loc; 
		}
	}

	return nearestLoc;
}

Location LogicalAgent::findClosestSafeToTarget()
{
	int nearestTargetDistance = 100;
	int nearestDistance = 100;
	Location nearestLoc = Location(0,0);
	
	for (int i = 0; i < this->safeFrontier.size() + this->safeVisited.size(); i++)
	{
		Location loc;
		if (i < this->safeFrontier.size()) { loc = this->safeFrontier[i]; }
		else { loc = this->safeVisited[i - this->safeFrontier.size()]; }
		 
		// make sure at least one axis is the same
		if (loc.X == this->target.X || loc.Y == this->target.Y)
		{
			int targetDistX = abs(loc.X - this->target.X);
			int targetDistY = abs(loc.Y - this->target.Y);
			int targetDistance = targetDistX + targetDistY;
			
			
			int distX = abs(loc.X - this->x);
			int distY = abs(loc.Y - this->y);
			int distance = distX + distY;

			if ((distance + targetDistance) < (nearestTargetDistance + nearestDistance))
			{ 
				nearestTargetDistance = targetDistance; 
				nearestDistance = distance;
				nearestLoc = loc; 
			}
		}
	}

	return nearestLoc;
}

// debugging function
void LogicalAgent::printList(vector<Location> list, string listName)
{
	cout << "\n" << listName << endl;
	for (int i = 0; i < list.size(); i++)
	{
		Location loc = list[i];
		cout << loc.X << "," << loc.Y << endl;
	}
}

void LogicalAgent::determineStrategy()
{
	Location current = Location(this->x, this->y);
	Location nearestSafe = findNearestSafeFrontier();
	
	if (this->safeFrontier.size() == 0)
	{
		// if we think a wumpus is blocking the way, we can try to take him out
		if (
			(!this->opInProgress || (this->opInProgress && this->op != "KILL" && this->op != "grab")) && 
			!this->wumpusDead && !this->shot &&
			(this->potentialWumpusLocations.size() > 0 || this->wumpusLocated)) 
		{ wumpusGoinDown(); }
		
		if (!this->opInProgress) { flipTableImOut(); }
	}
	if (!this->opInProgress)
	{
		if (supmuwLocated && !foodObtained)
		{
			this->op = "food";
			this->stage = "movement";
			vector<Location> movements = greedyASTARthing(current, this->knownSupmuwLocation, false);
			this->targetLoc = this->knownSupmuwLocation;
			this->movementPath = movements;
			this->strategy.clear();
		}
		else
		{
			// move to closest safe spot
			this->opInProgress = true;
			this->op = "explore";
			this->stage = "movement";
			vector<Location> movements = greedyASTARthing(current, nearestSafe, false);
			this->targetLoc = nearestSafe;
			this->movementPath = movements;
			this->strategy.clear();
			//printList(movements, "movements");
		}
	}
}

Action LogicalAgent::carryOutStrategy()
{
	if (!this->stratPrinted)
	{
		cout << "Strategy: " << this->op << endl;
		cout << "Stage: " << this->stage << endl;
		this->stratPrinted = true;
	}
	Action action;
	this->actionAssigned = false;
	if (this->opInProgress)
	{
		if (this->op == "grab")
		{
			if (this->stage == "grab")
			{
				action = GRAB;
				actionAssigned = true;

				this->stage = "movement";
				this->targetLoc = Location(1,1);
				vector<Location> movements = greedyASTARthing(Location(this->x, this->y), this->targetLoc, true);
				this->movementPath = movements;
				this->strategy.clear();
			}
			if (this->stage == "out")
			{
				if (Location(this->x, this->y) == Location(1,1))
				{
					action = CLIMB;
					actionAssigned = true;
				}
				else
				{
					this->op = "out";
					this->stage = "movement";

					this->targetLoc = Location(1,1);
					vector<Location> movements = greedyASTARthing(Location(this->x, this->y), this->targetLoc, true);
					this->movementPath = movements;
					this->strategy.clear();
					
					this->opInProgress = true;
				}
			}
		}
		else if (this->op == "run")
		{
			if (this->stage == "out")
			{
				if (Location(this->x, this->y) == Location(1,1))
				{
					action = CLIMB;
					actionAssigned = true;
				}
				else
				{
					this->op = "run";
					this->stage = "movement";

					this->targetLoc = Location(1,1);
					vector<Location> movements = greedyASTARthing(Location(this->x, this->y), this->targetLoc, true);
					this->movementPath = movements;
					this->strategy.clear();
					
					this->opInProgress = true;
				}
			}
		}
		else if (this->op == "KILL")
		{
			if (this->stage == "orient")
			{
				action = this->strategy[0];
				this->strategy.erase(this->strategy.begin());

				if (action == GOFORWARD)
				{
					action = SHOOT;
					this->stage = "check"; // check if we actually killed (scream)
				}

				actionAssigned = true;
			}
			else if (this->stage == "check")
			{
				if (!doListHazLoc(this->potentialPitLocations,this->target)) { this->safeFrontier.push_back(this->target); }
				if (this->wumpusDead) 
				{ 
					for (int i = 0; i < this->potentialWumpusLocations.size(); i++)
					{
						Location loc = this->potentialWumpusLocations[i];
						if (!doListHazLoc(this->potentialPitLocations, loc) && !doListHazLoc(this->safeVisited, loc) && !doListHazLoc(this->safeFrontier, loc)) { this->safeFrontier.push_back(loc); }
					}
					this->potentialWumpusLocations.clear(); 
					//this->expandSafeFrontier(this->target);
				}
				else
				{
					removeFromList(&(this->potentialWumpusLocations), this->target);
					if (this->potentialWumpusLocations.size() == 1)
					{
						this->knownWumpusLocation = this->potentialWumpusLocations[0];
						this->wumpusLocated = true;
					}
				}
				this->opInProgress = false;
				determineStrategy();
				return carryOutStrategy();
			}
		}
	
		if (this->stage == "movement" && !this->actionAssigned)
		{
			bool stop = false;

			if (Location(this->x, this->y) == this->targetLoc) { stop = true; }

			// if we've done turns and move, get next loc move
			if (this->strategy.size() == 0)
			{

				if (this->movementPath.size() == 0) { stop = true; }
				else
				{
					// "pop" next move
					Location nextLoc = this->movementPath[0];
					this->movementPath.erase(this->movementPath.begin());

					vector<Action>* actions = calcMoveToAdjacent(nextLoc);
					this->strategy = *actions;
				}
			}

			if (!stop)
			{
				// get the next action to take ("pop" off strategy)
				action = this->strategy[0];
				this->strategy.erase(this->strategy.begin());
				
				actionAssigned = true;
			}
			else // stop
			{
				// stage advancement
				this->strategy.clear();
				this->movementPath.clear();
				if (this->op == "explore") 
				{ 
					this->opInProgress = false;
					determineStrategy();
					return carryOutStrategy();
				}
				else if (this->op == "KILL")
				{
					this->stage = "orient"; 
					this->strategy = *(calcMoveToAdjacent(this->target));
					actionAssigned = false;
					return carryOutStrategy();
				}
				else if (this->op == "run" || this->op == "grab") { this->stage = "out"; return carryOutStrategy(); }
			}
		}
	
	}
	return action;
}

Action LogicalAgent::Process(Percept& percept)
{
	Action action;
	this->actionAssigned = false;
	this->stratPrinted = false;

	if (percept.Bump) { owThatsAWall(); }
	if (percept.Glitter) { grabTheMoneyAndRun(); }
	if (percept.Scream) { this->wumpusDead = true; }

	// check current location
	Location current = Location(this->x, this->y);
	if (!doListHazLoc(this->safeVisited, current))
	{
		// take out of frontier and put into safe visited
		removeFromList(&(this->safeFrontier), current);
		this->safeVisited.push_back(current);

		// if this spot was in potential lists, remove
		removeFromList(&(this->potentialWumpusLocations), current);
		removeFromList(&(this->potentialPitLocations), current);

		// check pits
		if (percept.Breeze) { handleNewBreeze(current); }
		else { removePotentialPits(current); }

		// check stench
		if (percept.Stench) { handleNewStench(current); }
		else { removePotentialWumpuses(current); 	}

		// check moo
		if (percept.Moo) { handleNewMoo(current); }
		else { removePotentialSupmuws(current); }

		if (!this->wumpusLocated) { triangulateStenches(); }
		//if (!this->supmuwLocated) { triangulateMoos(); }

		// check if we can expand safe frontier
		if (!percept.Breeze && (!percept.Stench || (percept.Stench && this->wumpusLocated))) { expandSafeFrontier(current); }
	}


	determineStrategy();
	action = carryOutStrategy();
	if (!this->opInProgress && !actionAssigned)
	{
		determineStrategy();
		action = carryOutStrategy();
	}
	
/*	while (!actionAssigned)
	{
		determineStrategy();
		action = carryOutStrategy();
	}*/
	
	/*cout << "hit enter to advance" << endl;
	cin.ignore();
	cin.clear();*/
	
	if (action == GOFORWARD)
	{
		if (this->heading == 0) { this->y++; }
		else if (this->heading == 1) { this->x++; }
		else if (this->heading == 2) { this->y--; }
		else if (this->heading == 3) { this->x--; }
	}
	else if (action == TURNRIGHT)
	{
		this->heading++;
		if (this->heading > 3) { this->heading = 0; }
	}
	else if (action == TURNLEFT)
	{
		this->heading--;
		if (this->heading < 0) { this->heading = 3; }
	}
	else if (action == SHOOT) { this->shot = true; }

	return action;
}

void LogicalAgent::GameOver(int score) {}

Agent* LogicalAgent::clone() { return new LogicalAgent(*this); }
