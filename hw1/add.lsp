(defun add (a b)
  (if (= a 0)
	b
	(add (- a 1) (+ b 1))))
