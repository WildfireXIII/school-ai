// HumanAgent.cpp
 
#include <iostream>
#include "HumanAgent.h"
//#include "Agent.h"

using namespace std;

HumanAgent::HumanAgent() { }

HumanAgent::~HumanAgent() { }

void HumanAgent::Initialize() { }

Action HumanAgent::Process(Percept& percept)
{
	char c;
	Action action;
	bool validAction = false;

	while (! validAction)
	{
		validAction = true;
		cout << "Action? ";
		cin >> c;
		if (c == 'f') { action = GOFORWARD; }
		else if (c == 'l') { action = TURNLEFT; }
		else if (c == 'r') { action = TURNRIGHT; }
		else if (c == 'g') { action = GRAB; }
		else if (c == 's') { action = SHOOT; }
		else if (c == 'c') { action = CLIMB; }
		else 
		{
			cout << "Huh?" << endl;
			validAction = false;
		}
	}
	return action;
}

void HumanAgent::GameOver(int score) { }

Agent* HumanAgent::clone() { return new HumanAgent(*this); }
