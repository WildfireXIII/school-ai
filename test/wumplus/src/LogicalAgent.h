// LogicalAgent.h

#ifndef LOGICAL_AGENT_H
#define LOGICAL_AGENT_H

#include "Action.h"
#include "Percept.h"
#include "Location.h"
#include "Agent.h"
#include <vector>
#include <stdlib.h>
#include <string>

class LogicalAgent: public Agent
{
private: 
	vector<Location> knownPitLocations;
	vector<Location> potentialPitLocations;
	vector<Location> breezeLocations;
	
	bool wumpusLocated = false;
	bool wumpusDead = false;
	bool shot = false;
	Location knownWumpusLocation;
	Location target;
	Location targetLoc;
	vector<Location> potentialWumpusLocations;
	vector<Location> stenchLocations;
	
	bool foodObtained = false;
	bool supmuwLocated = false;
	Location knownSupmuwLocation;
	vector<Location> potentialSupmuwLocations;
	vector<Location> mooLocations;

	vector<Location> knownWalls;

	vector<Location> safeFrontier;
	vector<Location> safeVisited; // known safe?
	
	int x;
	int y;
	int heading; // 0 = north, 1 = east, 2 = south, 3 = west

	int turn;

	bool opInProgress = false;
	bool movementComplete = false;
	string op;
	string stage;
	vector<Action> strategy;
	vector<Location> movementPath;
	bool actionAssigned;

	bool stratPrinted = false;
	
public:
	
	LogicalAgent();
	~LogicalAgent();
	void Initialize();
	Action Process(Percept& percept);
	void GameOver(int score);
	Agent* clone();

	bool doListHazLoc(vector<Location> list, Location loc);
	void removeFromList(vector<Location>* list, Location loc);
	int Ldistance(int x_loc, int y_loc, int x_goal, int y_goal);
	vector<Location> safeAdjacents(Location loc, bool guarenteed);
	vector<Location> greedyASTARthing(Location start, Location end, bool guarenteed);
	//vector<Location> greedyASTARthing(Location start, Location end);

	void owThatsAWall();
	void grabTheMoneyAndRun();
	void flipTableImOut();
	void wumpusGoinDown();

	void handleNewMoo(Location loc);
	void removePotentialSupmuws(Location loc);
	void triangulateMoos();

	void handleNewBreeze(Location loc);
	void removePotentialPits(Location loc);

	void handleNewStench(Location loc);
	void triangulateStenches();
	void removePotentialWumpuses(Location loc);

	void expandSafeFrontier(Location loc);

	Location findNearestSafeFrontier();

	Location findClosestSafeToTarget();

	vector<Action>* calcMoveToAdjacent(Location loc);


	void printList(vector<Location> list, string listName);

	Action manualAction(Percept& percept);

	void determineStrategy();
	Action carryOutStrategy();
};


#endif
