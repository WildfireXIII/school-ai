// Agent.h

#ifndef AGENT_H
#define AGENT_H

#include "Action.h"
#include "Percept.h"


class Agent
{
public:
	Agent() {}
	virtual ~Agent() {}
	virtual void Initialize()=0;
	virtual Action Process(Percept& percept)=0;
	virtual void GameOver(int score)=0;
	virtual Agent* clone()=0;
};

#endif // AGENT_H
